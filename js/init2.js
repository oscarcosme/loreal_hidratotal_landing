var hidden, visibilityChange;
if (typeof document.hidden !== "undefined") {
    hidden = true;
    visibilityChange = "visibilitychange";
} else if (typeof document.mozHidden !== "undefined") {
    hidden = true;
    visibilityChange = "mozvisibilitychange";
} else if (typeof document.msHidden !== "undefined") {
    hidden = true;
    visibilityChange = "msvisibilitychange";
} else if (typeof document.webkitHidden !== "undefined") {
    hidden = true;
    visibilityChange = "webkitvisibilitychange";
}

document.addEventListener(visibilityChange, handleVisibilityChange, false);
function handleVisibilityChange() {
	if (document.hidden) {
		audioElement.pause();
	} else  {
		audioElement.play();
	}
}

//Destruimos la animación para mobile
var isMobile = window.matchMedia("screen and (min-width: 768px)");

function detectmob() { 
	if( navigator.userAgent.match(/Android/i)
		|| navigator.userAgent.match(/webOS/i)
		|| navigator.userAgent.match(/iPhone/i)
		|| navigator.userAgent.match(/iPad/i)
		|| navigator.userAgent.match(/iPod/i)
		|| navigator.userAgent.match(/BlackBerry/i)
		|| navigator.userAgent.match(/Windows Phone/i)
	){
		return true;
	}else {
		return false;
	}
}


	function iniciar(tab) {

		switch(tab) {

			case "scene-03":
				group13.play();
				break;
			case "scene-04":
				group14.play();
				break;
			case "scene-05":
				group15.play();
				break;
			case "scene-06":
				group16.play();
				break;
			case "scene-07":
				group17.play();
				break;
			case "scene-08":
				group18.play();
				break;
			case "scene-09":
				group19.play();
				break;
			case "scene-10":
				group20.play();
				break;
			case "scene-11":
				group21.play();
				break;
			case "scene-12":
				break;
			case "scene-13":
				group24.play();
				break;
			case "scene-14":
				group24.play();
				break;
		}
	}

$(function() {
	$('.faceShare').on('click',function(){
		share();
	});
	$('.twShare').on('click',function(){
		shareTw();
	});
	$('.scene-15 p').css('opacity','1');
	$('.scene-15').show();

	var scene_03 = function(){
		setTimeout(function(){
			$('.scene-02').fadeOut('slow');
			$('.scene-03').fadeIn('slow');
			
			$('.scene-03').addClass('run');

			if($('.step').hasClass('run')){
				iniciar('scene-03');
			}
		},4000);
	}
	
	var scene_04 = function(){
		$('.scene-03').fadeOut('slow');
		$('.scene-04').fadeIn('slow');
		
		$('.scene-03').removeClass('run');
		$('.scene-04').addClass('run');

		if($('.step').hasClass('run')){
			iniciar('scene-04');
		}

	}

	var scene_05 = function(){

		$('.scene-04').fadeOut('slow');
		$('.scene-05').fadeIn('slow');
		
		$('.scene-04').removeClass('run');
		$('.scene-05').addClass('run');

		if($('.step').hasClass('run')){
			iniciar('scene-05');
		}

	}

	var scene_06 = function(){
		$('.scene-05').fadeOut('slow');
		$('.scene-06').fadeIn('slow');
		
		$('.scene-05').removeClass('run');
		$('.scene-06').addClass('run');

		if($('.step').hasClass('run')){
			iniciar('scene-06');
		}

	}

	var scene_07 = function(){
		$('.scene-06').fadeOut('slow');
		$('.scene-07').fadeIn('slow');
		
		$('.scene-06').removeClass('run');
		$('.scene-07').addClass('run');
	$('.scene-13, .scene-14').hide();

		if($('.step').hasClass('run')){
			iniciar('scene-07');
		}

	}

	var scene_08 = function(){
		$('.scene-07').fadeOut('slow');
		$('.scene-08').fadeIn('slow');
		
		$('.scene-07').removeClass('run');
		$('.scene-08').addClass('run');
	$('.scene-13, .scene-14').hide();

		if($('.step').hasClass('run')){
			iniciar('scene-08');
		}

	}

	var scene_09 = function(){
		$('.scene-08').fadeOut('slow');
		$('.scene-09').fadeIn('slow');
		
		$('.scene-08').removeClass('run');
		$('.scene-09').addClass('run');
	$('.scene-13, .scene-14').hide();

		if($('.step').hasClass('run')){
			iniciar('scene-09');
		}
	}

	var scene_10 = function(){
		$('.scene-09').fadeOut('slow');
		$('.scene-10').fadeIn('slow');
		
		$('.scene-09').removeClass('run');
		$('.scene-10').addClass('run');
	$('.scene-13, .scene-14').hide();

		if($('.step').hasClass('run')){
			iniciar('scene-10');
		}
	}

	var scene_11 = function(){
		$('.scene-10').fadeOut('slow');
		$('.scene-11').fadeIn('slow');
		
		$('.scene-10').removeClass('run');
		$('.scene-11').addClass('run');
	$('.scene-13, .scene-14').hide();

		if($('.step').hasClass('run')){
			iniciar('scene-11');
		}
	}

	var scene_12 = function(){
		$('.scene-11').fadeOut('slow');
		$('.scene-13').fadeOut('slow');
		$('.scene-14').fadeIn('slow');
		
		$('.scene-11').removeClass('run');
		$('.scene-13').removeClass('run');
		$('.scene-14').addClass('run');
		$('.scene-13, .scene-14').hide();
		$('.scene-15').fadeIn('slow');

		/*if($('.step').hasClass('run')){
			iniciar('scene-14');
		}*/
	}

	//VARIABLES
	var group1 = new TimelineMax({paused:true});
		group2 = new TimelineMax({paused:true});
		group3 = new TimelineMax({paused:true});
		group4 = new TimelineMax({paused:true});
		group5 = new TimelineMax({paused:true});

		group10 = new TimelineMax({paused:true});
		group11 = new TimelineMax({paused:true});
		group12 = new TimelineMax({paused:true , onComplete: scene_03});
		group13 = new TimelineMax({paused:true , onComplete: scene_04});
		group14 = new TimelineMax({paused:true , onComplete: scene_05});
		group15 = new TimelineMax({paused:true , onComplete: scene_06});
		group16 = new TimelineMax({paused:true , onComplete: scene_07});
		group17 = new TimelineMax({paused:true , onComplete: scene_08});
		group18 = new TimelineMax({paused:true , onComplete: scene_09});
		group19 = new TimelineMax({paused:true , onComplete: scene_10});
		group20 = new TimelineMax({paused:true , onComplete: scene_11});
		group21 = new TimelineMax({paused:true , onComplete: scene_12});
		//group22 = new TimelineMax({paused:true , onComplete: scene_13});
		group23 = new TimelineMax({paused:true});
		group24 = new TimelineMax({paused:true});
		
	var player;
	$('#cargando').hide();
	$('.thumb, .text, .year').attr('style','');
	$('.scene').css('opacity','1');
	/*$('.share.regist').on('click', function(e){
		e.preventDefault();
		console.log('da');
		setTimeout(function(){
			window.location.href = "http://www.lorealparis-ar.com.ar/Brands/Cuidado-De-La-Piel/Anti-Edad";
		}, 500);
	});*/

	if(navigator.userAgent.match(/iPhone/i)
		|| navigator.userAgent.match(/iPad/i)
		|| navigator.userAgent.match(/iPod/i)) {
		setTimeout(function(){
			$('#cargando').hide();
			$('.thumb, .text, .year').attr('style','');
			$('.scene').css('opacity','1');
		},1000);
	}

	$('.participa').flowtype({
		minimum   : 500, 
		maximum   : 1200, 
		minFont   : 12, 
		maxFont   : 40, 
		fontRatio : 30
	});
	//ANIMANDO
	/*ANIMACION*/
	group1
		.from(".home .container p", 1, {opacity:0, y:-20, ease:Back.easeOut}, 0.1)
		.from(".home .container ul.edad li:nth-child(1) a", 1, {opacity:0, y:40, ease:Back.easeOut}, 0.5)
		.from(".home .container ul.edad li:nth-child(2) a", 1, {opacity:0, y:40, ease:Back.easeOut}, 1)
		.from(".home .container ul.edad li:nth-child(3) a", 1, {opacity:0, y:40, ease:Back.easeOut}, 1.5)
		.from(".home .container h3", 1, {opacity:0, y:-20, ease:Back.easeOut}, 2)

	group2
		.from(".two .left .brand", .5, {opacity:0, y:-20, ease:Back.easeOut}, 0.1)
		.from(".two .left div:last-child", .5, {opacity:0, x:-20, ease:Back.easeOut}, 0.3)

	group3
		.from(".three .left .brand", .5, {opacity:0, y:-20, ease:Back.easeOut}, 0.1)
		.from(".three .left div:last-child", .5, {opacity:0, x:-20, ease:Back.easeOut}, 0.3)

	group4
		.from(".four .left .brand", .5, {opacity:0, y:-20, ease:Back.easeOut}, 0.1)
		.from(".four .left div:last-child", .5, {opacity:0, x:-20, ease:Back.easeOut}, 0.3)

	group5
		.from(".five .left .biography", 1, {opacity:0, y:50, ease:Back.easeOut}, 0.5)


	group10
		.from(".participa .scene-01 .container p", 1, {opacity:0, y:-20}, 0.1)
		.from(".participa .scene-01 .container span", 1, {opacity:0}, 0.3)
	if(isMobile.matches){
		group12
			.from(".scene-02 .thumb-01", 3, {opacity:0, right:'-20%', y:-100, scale:7}, 0.1)
			.from(".scene-02 .thumb-04", 4, {opacity:0, left:'-50%', y:-20, scale:7}, 3)
			.from(".scene-02 .thumb-02", 3, {opacity:0, right:'0%', y:100, scale:5.7}, 2)
			.from(".scene-02 .thumb-03", 4, {opacity:0, y:300, x:50, scale: 8}, 1)
			.from(".scene-02 .year", 1, {opacity:0, scale:1.2}, 4.5)

		group13
			.from(".scene-03 .thumb-01", 3, {opacity:0, top:'90%', right:'100%', scale:5}, 2)//down
			.from(".scene-03 .thumb-03", 4, {opacity:0, top:'-120%', right:'-150%', scale:6}, 3)//top right
			.from(".scene-03 .thumb-02", 4, {opacity:0, top:'-120%', left:'-50%', scale:8}, 4)//top left
			.from(".scene-03 .text", 2, {opacity:0, y:20, scale:.8, ease:Back.easeOut}, 6)

			.to(".scene-03 .thumb-01", 3, {y:-60, x:-30}, 2)
			.to(".scene-03 .thumb-02", 4, {y:-50, x:50}, 2)
			.to(".scene-03 .thumb-03", 3, {y:50, x:-30}, 2)

			.to(".scene-03 .thumb-01", 2.5, {top:'150%'}, 9)
			.to(".scene-03 .thumb-02", 2.5, {top:'150%'}, 9.2)
			.to(".scene-03 .thumb-03", 2.5, {top:'150%'}, 9.3)
			.to(".scene-03 .text", 3, {opacity:0, ease:Back.easeIn}, 7.5)

		group14
			.from(".scene-04 .thumb-01", 3, {opacity:0, top:'-90%', left:'-50%', scale:6}, 2)
			.from(".scene-04 .thumb-02", 4, {opacity:0, top:'80%', left:'150%', scale:6}, 3)
			.from(".scene-04 .thumb-03", 3, {opacity:0, top:'140%', left:'-80%', scale:7}, 4)
			.from(".scene-04 .text", 2, {opacity:0, y:20, ease:Back.easeOut}, 6)

			.to(".scene-04 .text", 3, {opacity:0, y:20, ease:Back.easeIn, delay:1}, 9)
			.to(".scene-04 .thumb-01", 3, {top:'150%', delay:1}, 10.2)
			.to(".scene-04 .thumb-02", 3, {top:'150%', delay:1}, 10.5)
			.to(".scene-04 .thumb-03", 3, {top:'150%', delay:1}, 10)
			
		group15
			.from(".scene-05 .thumb-01", 4, {opacity:0, right:'-20%', y:-100, scale:7}, 1)
			.from(".scene-05 .thumb-02", 5, {opacity:0, right:'0%', y:100, scale:5.7}, 3)
			.from(".scene-05 .thumb-03", 4, {opacity:0, y:300, x:50, scale: 8}, 2)
			.from(".scene-05 .thumb-04", 5, {opacity:0, left:'-50%', y:-20, scale:7}, 4)
			.from(".scene-05 .year", 2, {opacity:0, scale:1.2}, 5)

		group16
			.from(".scene-06 .thumb-01", 3, {opacity:0, top:'90%', left:'100%', scale:7}, 1)
			.from(".scene-06 .thumb-02", 3, {opacity:0, top:'-120%', left:'-50%', scale:8}, 2)
			.from(".scene-06 .text", 2, {opacity:0, y:20, scale:.7, ease:Back.easeOut}, 4)

			.to(".scene-06 .thumb-01", 2, {y:-60, x:-30}, 2)
			.to(".scene-06 .thumb-02", 4, {y:-50, x:50}, 2)

			.to(".scene-06 .thumb-01", 2.5, {top:'150%'}, 9)
			.to(".scene-06 .thumb-02", 2.5, {top:'150%'}, 9.2)
			.to(".scene-06 .text", 3, {opacity:0, ease:Back.easeIn}, 7.5)

		group17
			.from(".scene-07 .thumb-01", 3, {opacity:0, top:'-90%', left:'-100%', scale:7}, 1)
			.from(".scene-07 .thumb-02", 5, {opacity:0, top:'-120%', left:'-150%', scale:8}, 2)
			.from(".scene-07 .thumb-03", 2, {opacity:0, top:'120%', left:'150%', scale:6}, 3)
			.from(".scene-07 .text", 2, {opacity:0, y:20, scale:1.4, ease:Back.easeOut}, 4)

			.to(".scene-07 .thumb-01", 3, {y:-60, x:-30}, 2)
			.to(".scene-07 .thumb-02", 2, {y:-50, x:50}, 2)
			.to(".scene-07 .thumb-03", 4, {y:50, x:-30}, 2)

			.to(".scene-07 .thumb-01", 2.5, {top:'150%'}, 9)
			.to(".scene-07 .thumb-02", 2.5, {top:'150%'}, 9.2)
			.to(".scene-07 .thumb-03", 2.5, {top:'150%'}, 9.2)
			.to(".scene-07 .text", 3, {opacity:0, ease:Back.easeIn}, 7.5)

		group18
			.from(".scene-08 .text", 2, {opacity:0, y:20, scale:.8, ease:Back.easeOut}, 3)
			.from(".scene-08 .thumb-01", 3, {opacity:0, top:'-90%', left:'100%', scale:7}, 0.1)
			.from(".scene-08 .thumb-02", 5, {opacity:0, top:'120%', left:'-150%', scale:8}, 0.5)

			.to(".scene-08 .thumb-01", 3, {y:-60, x:-30}, 2)
			.to(".scene-08 .thumb-02", 2, {y:-50, x:50}, 2)

			.to(".scene-08 .thumb-01", 2.5, {top:'150%'}, 9)
			.to(".scene-08 .thumb-02", 2.5, {top:'150%'}, 9.2)
			.to(".scene-08 .text", 3, {opacity:0, ease:Back.easeIn}, 7.5)

		group19
			.from(".scene-09 .text", 2, {opacity:0, y:20, scale:.8, ease:Back.easeOut}, 2)
			.from(".scene-09 .thumb-01", 3, {opacity:0, top:'90%', left:'100%', scale:.7, ease:Back.easeOut}, 0.5)
			
			.to(".scene-09 .text", 3, {opacity:0, ease:Back.easeIn}, 7.5)

		group20
			.from(".scene-10 .thumb-01", 3, {opacity:0, top:'-90%', left:'100%', scale:7}, 0.1)
			.from(".scene-10 .thumb-02", 5, {opacity:0, top:'120%', right:'-150%', scale:8}, 0.5)
			.from(".scene-10 .text", 1, {opacity:0, y:20, scale:.8, ease:Back.easeOut}, 4)
			.from(".scene-10 .year", 1, {opacity:0, scale:1.2}, 1.5)

			.to(".scene-10 .thumb-01", 3, {y:-60, x:-30}, 3)
			.to(".scene-10 .thumb-02", 2, {y:-50, x:50}, 3)

			.to(".scene-10 .thumb-01", 2.5, {top:'150%'}, 9)
			.to(".scene-10 .thumb-02", 2.5, {top:'150%'}, 9.2)
			.to(".scene-10 .text", 3, {opacity:0, ease:Back.easeIn}, 7.5)

		group21
			.from(".scene-11 .thumb-01", 3, {opacity:0, top:'90%', left:'100%', scale:7}, 0.1)
			.from(".scene-11 .text", 1, {opacity:0, y:20, scale:.8, ease:Back.easeOut}, 3)

			.to(".scene-10 .thumb-01", 10, {y:-60, x:-30}, 3)

		/*group22
			.from('.scene-12 p.mensaje', 1, {opacity:0, y:-40}, 0.5)
			.from('.father .left', 1, {opacity:0, x:-40}, 1)
			.from('.father .right', 1, {opacity:0, x:40}, 1)
			.from('.father p.hash', 1, {opacity:0, scale:.9, ease:Back.easeOut}, 1.5)

		group23
			.from('.scene-13 img', 1, {opacity:0, scale:.8, ease:Back.easeOut}, .5)
			.from('.scene-13 p', 1, {opacity:0, y:-20}, 1)
			.from('.scene-13 a', 1, {opacity:0, y:20}, 1.5)*/

		group24
			.from('.scene-14 p', 1, {opacity:0, scale:.7}, 0.5)

	}
	octopusFace.iniciarAnimacion();
});

// MOBILE
var refreshIntervalId = 0;
var scene = 2;
function movie(){
	refreshIntervalId = setInterval(function(){
		var strscene = (scene<=9)?'0'+scene:scene;
		
		var scene_Sig = scene + 1;
		var strscene2 = (scene_Sig<=9)?'0'+scene_Sig:scene_Sig;

		if (scene_Sig >= 12){
			$('.scene-11').fadeOut('slow');
			$('.scene-13').hide();
			$('.scene-14').hide();
			$('.scene-15').show();
			clearInterval(refreshIntervalId);
			refreshIntervalId = 0;
		} else {
			$('.scene-'+strscene).fadeOut('slow');
			$('.scene-'+strscene2).fadeIn('slow');
		}

		scene++;

		if (scene_Sig >= 13){
			clearInterval(refreshIntervalId);
			refreshIntervalId = 0;
		}
	}, 5000);
}
audioElement = document.getElementById("audio");

$(function(){
	$('#activarSonido').show();
	$('#activarSonido').on('click', function(){
		if (!$(this).hasClass('desactivado')) {
			$(this).addClass('desactivado');
			audioElement.pause();
		} else {
			$(this).removeClass('desactivado');	
			audioElement.play();
		}
	})
})
var octopusFace = {
	iniciarAnimacion: function() {
		$('.scene-02').show();

		if($(document).width()>768) {
			group12.play();
		} else {
			movie();
		}
        audioElement.play();
	}
}

function share() {
	FB.ui({
	  method: 'feed',
	  link: window.location.href,
	  picture: 'http://c0140440.ferozo.com/Share.jpg'
	}, function(response){
		
	});
}
function shareTw(){
  var link = window.location.href;
  var text = 'La historia de mi piel ¿Sentís que cambiaste mucho en estos años? #TuEdad #TuCrema';
  window.open('http://twitter.com/share?original_referer='+encodeURIComponent(link)+'&text='+encodeURIComponent(text)+'&url='+link,'Tweet','toolbar=0,status=0,width=468,height=404');
}
window.fbAsyncInit = function() {
	FB.init({
		//appId      : '383328205388361',  //TEST
		appId      : '378832855837896',  //PROD
		xfbml      : true,
		version    : 'v2.8'
	});
};

(function(d, s, id){
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) {return;}
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/en_US/sdk.js";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
