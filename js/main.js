$(function(){
	setTimeout(function(){
		$('body').addClass('loaded');
	}, 2000);

	$.scrollify({
        section : ".panel",
        sectionName : "section-name",
        easing: "easeOutExpo",
        scrollSpeed: 1100,
        offset : 0,
        scrollbars: true,
        standardScrollElements: false,
        setHeights: true,
        overflowScroll: true,
        before:function(i,panels) {
			var ref = panels[i].attr("data-section-name");
			$(".pagination .active").removeClass("active");
			$(".pagination").find("a[href=\"#" + ref + "\"]").addClass("active");
        },
        after:function(i) {
        },
        afterResize:function() {},
        afterRender:function() {}
    });
	$.scrollify.disable();
	$(".pagination a").on("click",$.scrollify.move);

});