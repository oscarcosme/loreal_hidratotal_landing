<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require 'vendor/autoload.php';
require 'db.php';
$config = [
    'settings' => [
        'displayErrorDetails' => true,


    ],
];
$app = new \Slim\App($config);

$app->post('/form', function (Request $request, Response $response, $args) {
	$data = $request->getParsedBody();


	$result = Db::getInstance()->select('form', [ 
		'id',
	],[ 
		'email' => strtolower($data['mail'])
	]);
	if (sizeof($result) > 0) {
		$vuelta = array('mensaje' => 'El email ya se encuentra registrado.', 'error' => true);
	} else {

		Db::getInstance()->insert('form', [
			'nombre' => filter_var($data['name'], FILTER_SANITIZE_STRING),
		    'email' => strtolower(filter_var($data['mail'], FILTER_SANITIZE_STRING)),
			'apellido' => filter_var($data['lastname'], FILTER_SANITIZE_STRING),
			'telefono' => filter_var($data['phone'], FILTER_SANITIZE_STRING),
			'suscripcion' => filter_var($data['check1'], FILTER_SANITIZE_STRING),
			'piel' => filter_var($data['piel'], FILTER_SANITIZE_STRING),
			'arrugas' => filter_var($data['arrugas'], FILTER_SANITIZE_STRING),
			'compra' => filter_var($data['compra'], FILTER_SANITIZE_STRING),
			'animacion' => filter_var($data['animacion'], FILTER_SANITIZE_STRING)
		]);

		if (Db::getInstance()->error() != ["00000",null,null]) {
			if (Db::getInstance()->error()[1] == 1062) {
				$vuelta = array('mensaje' => 'El email ya se encuentra registrado.', 'error' => true);
			} else { 
				$vuelta = array('mensaje' => 'Ocurrio un error, intentalo nuevamente.', 'error' => true);
			}
		} else {
			$vuelta = array('mensaje' => 'OK', 'error' => false);
		}
	}
    return $response->withJson($vuelta);
});

$app->post('/guardaranimacion', function (Request $request, Response $response, $args) {
	$data = $request->getParsedBody();
	$cabecera = '<!DOCTYPE html>
					<html lang="en">
					<head>
						<meta charset="UTF-8">
						<meta http-equiv="x-ua-compatible" content="ie=edge">
						<title>Loreal</title>
						<meta name="viewport" content="width=device-width, initial-scale=1">
						<link rel="stylesheet" href="../css/normalize.css">
						<link rel="stylesheet" href="../css/magnific-popup.css">
						<link rel="stylesheet" href="../css/main.css">
						<link rel="stylesheet" href="../css/queries.css">
						<script src="../js/modernizr-2.8.3.min.js"></script>
						<meta property="og:url" content="" />
						<meta property="og:site_name" content="">
						<meta property="og:title" content="La historia de mi piel" />
						<meta property="og:type" content="website"/>
						<meta property="og:description" content="¿Sentís que cambiaste mucho en estos años? #TuEdad #TuCrema" />
						<meta property="og:image" content="http://c0140440.ferozo.com/animaciones/Share.jpg" />
						<meta property="fb:app_id" content="378832855837896" />
					</head>
					<body>
						<div class="header">
							<div class="inner">
								<img src="../images/logo.png" alt="">
							</div>
						</div>
						<div class="panel panel5 participa" data-section-name="participa">';
	$footer = '	<div class="step scene-13 scene-15 waypoint-24">
					<div class="container">
						<p style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);">Armá tu video y participá por un año de cremas.</p><br/><br/>
						<a style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0); font-size: 20px; background: #000;color: #fff;text-transform: uppercase;width: 350px;font-size: 0.5em;padding: 14px 20px;display: block;" href="http://www.lorealparis-ar.com.ar/Brands/Cuidado-De-La-Piel/Anti-Edad" class="share regist">Participá</a>
						<p style="font-size: 0.7em;">Compartí esta animacion: <img class="faceShare" src="face.png" style="display: inline-block;width: 42px;vertical-align: bottom;cursor: pointer;"><img class="twShare" src="tw.png" style="display: inline-block;width: 50px;vertical-align: bottom;cursor: pointer;"></p>
					</div>
				</div>
				</div>
				<div id="cargando">
					<div class="header">
						<div class="inner">
							<img src="../images/logo.png" alt="">
						</div>			
					</div>
					<div class="contenedor">
						<div class="loader"></div>
						<p class="uno">CARGANDO</p>
						<p class="dos">Un momento por favor, estamos cargando las imágenes de tus álbumes.</p>
					</div>
				</div>
				<script src="../js/jquery-1.12.0.min.js"></script>
				<script src="../js/flowtype.js"></script>
				<script src="../js/jquery.scrollify.min.js"></script>
				<script defer src="../js/jquery.magnific-popup.js"></script>
				<script defer src="../js/jquery.validate.js"></script>
				<script defer src="../js/jquery.alphanum.js"></script>
				<script src="../js/jquery.waypoints.min.js"></script>
				<script src="../js/tweenmax.min.js"></script>
				<script defer src="../js/main.js"></script>
				<script defer src="../js/init2.js"></script>
			</body>
			</html>';

	$token = bin2hex(openssl_random_pseudo_bytes(11));
	$fh = fopen('../animaciones/'.$token.'.html', 'w') or die("can't open file"); 
	fwrite($fh, $cabecera.$data['html'].$footer); 
	fclose($fh);

	$vuelta = array('token' => $token.'.html' );
    return $response->withJson($vuelta);
});

$app->run();
